class Order < ActiveRecord::Base
  enum state: [ :active, :deleted ]
  validates_presence_of :oid, :category, :state
  validates :oid, uniqueness: true
  has_one :invoice
  has_one :patient, through: :invoice
  self.primary_key = 'oid'
end