class Invoice < ActiveRecord::Base
  enum state: [ :active, :deleted ]
  belongs_to :order, primary_key: 'oid'
  belongs_to :patient, primary_key: 'pid'
end