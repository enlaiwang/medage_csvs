class Patient < ActiveRecord::Base
  enum state: [ :active, :deleted ]
  validates_presence_of :pid, :name, :state
  validates :pid, uniqueness: true, length: {maximum: 7}
  has_many :invoices
  has_many :orders, through: :invoices
  self.primary_key = 'pid'
end
