class InvoicesController < ApplicationController

	def index
  	@invoices = Invoice.all
  end

  def show
  	@invoice = Invoice.find_by_id(params[:id])
    render json: @invoice
  end
end
