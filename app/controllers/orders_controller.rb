class OrdersController < ApplicationController
  def index
  	@orders = Order.all
  end

  def show
  	@order = Order.find_by_id(params[:id])
    render json: @order
  end
end
