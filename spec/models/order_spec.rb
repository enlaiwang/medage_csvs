require 'spec_helper'

describe Order do
	before do
		@order = FactoryGirl.build(:order)
	end

	describe 'Order has valid attributes' do
		it "should be valid" do
			expect(@order).to be_valid
		end
	end

	describe "when order id is empty" do
		it "should be invalid" do
			@order.oid=nil
			expect(@order).not_to be_valid
		end
	end
	
	context "update Order with invalid state" do
		it "raises argumentError" do
			expect{@order.update(state: "invalid")}.to raise_error(ArgumentError)
		end
	end
 end