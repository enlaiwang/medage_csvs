require 'spec_helper'

describe Patient do
	before do
		@patient = FactoryGirl.build(:patient)
	end

	describe 'patient has valid attributes' do
		it "should be valid" do
			expect(@patient).to be_valid
		end
	end

	describe "when patient id is empty" do
		it "should be invalid" do
			@patient.pid=nil
			expect(@patient).not_to be_valid
		end
	end

	describe "when patient id has more 7 characters" do
		it "should be invalid" do
			@patient.pid="U12345678"
			expect(@patient).not_to be_valid
		end
	end
	
	context "update Patient with invalid state" do
		it "raises argumentError" do
			expect{@patient.update(state: "invalid")}.to raise_error(ArgumentError)
		end
	end
 end