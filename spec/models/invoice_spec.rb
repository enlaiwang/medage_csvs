require 'spec_helper'

describe Invoice do
	before do
		@invoice = FactoryGirl.build(:invoice)
	end
	describe 'invoice has valid attributes' do
		it "should be valid" do
			expect(@invoice).to be_valid
		end
	end

	context "update invoice with invalid state" do
		it "raises argumentError" do
			expect{@invoice.update(state: "invalid")}.to raise_error(ArgumentError)
		end
	end
 end