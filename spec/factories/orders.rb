FactoryGirl.define do
  factory :order do
    oid 'C123456'
    category 'big mask'
    state 'active'
  end
end
