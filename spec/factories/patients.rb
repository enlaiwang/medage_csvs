FactoryGirl.define do
  factory :patient do
    pid 'U123456'
    name 'John Doe'
    state 'active'
  end
end
