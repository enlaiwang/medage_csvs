require 'csv'

class MedsageScvsParser

	def self.load_file(directory_name)

		Dir.glob("#{directory_name}/*.csv") do |file_name|
			begin
				CSV.foreach(file_name, headers: true, header_converters: :symbol) do |row|
					##This is the order object
					if row[:patient_id].nil? 
						order = Order.where(oid: row[:order_id]).first_or_initialize
						order.category = row[:category]
						order.state =  row[:state]
						order.save
					elsif row[:patient_name].nil?  ##This is the invoice object
						invoice = Invoice.where(order_id: row[:order_id], patient_id: row[:patient_id]).first_or_initialize
						invoice.state = row[:state]
						invoice.save
					else  #This is the patient object
						patient = Patient.where(pid: row[:patient_id]).first_or_initialize
						patient.name = row[:patient_name]
						patient.state = row[:state]
						patient.save
					end
				end
			rescue => e
				puts e.inspect
				puts "Failed to load the scvs file #{file_name}."
			end
		end
	end
end