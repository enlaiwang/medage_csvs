namespace :medsage do
  desc 'Load the data for medsage from scvs files'

  task :load_scvs, [:file] => :environment do |t, args|
      MedsageScvsParser.load_file(args.file)
  end

  desc 'generate all data'
  task all: :environment do
    Rake::Task['medsage:load_scvs'].invoke
  end
end