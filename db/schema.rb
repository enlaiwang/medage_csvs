# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160715142233) do

  create_table "invoices", force: :cascade do |t|
    t.string   "order_id",   limit: 255
    t.string   "patient_id", limit: 255
    t.integer  "state",      limit: 4,   default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoices", ["order_id"], name: "index_invoices_on_order_id", using: :btree
  add_index "invoices", ["patient_id"], name: "index_invoices_on_patient_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.string   "oid",        limit: 255,             null: false
    t.string   "category",   limit: 255
    t.integer  "state",      limit: 4,   default: 0, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "orders", ["oid"], name: "index_orders_on_oid", using: :btree

  create_table "patients", force: :cascade do |t|
    t.string   "pid",        limit: 7,              null: false
    t.string   "name",       limit: 45,             null: false
    t.integer  "state",      limit: 4,  default: 0, null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "patients", ["pid"], name: "index_patients_on_pid", using: :btree

end
