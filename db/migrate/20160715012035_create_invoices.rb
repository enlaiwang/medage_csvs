class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :order_id
      t.string :patient_id
      t.integer :state, default: 0, null: false
      t.timestamps
    end
  end
end
