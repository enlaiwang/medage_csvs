class RenameOrderIdForOrders < ActiveRecord::Migration
  def change
  	rename_column :orders, :order_id, :oid
  end
end
