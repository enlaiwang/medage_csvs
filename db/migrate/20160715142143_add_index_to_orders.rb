class AddIndexToOrders < ActiveRecord::Migration
  def change
  	add_index :orders, :oid
  end
end
