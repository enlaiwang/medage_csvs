class AddIndexToInvoices < ActiveRecord::Migration
  def change
  	add_index :invoices, :patient_id
  	add_index :invoices, :order_id
  end
end
