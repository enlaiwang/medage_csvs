class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :pid, limit: 7, null: false
      t.string :name, limit: 45, null: false
      t.integer :state, default: 0, null: false
      t.timestamps null: false
    end
  end
end
