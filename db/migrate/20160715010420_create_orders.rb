class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :order_id, null: false
      t.string :category
      t.integer :state, default: 0, null: false
      t.timestamps null: false
    end
  end
end
